<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class ModuleMake extends Command
{
    protected $files;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:module {name : Создать контроллер и файл маршрутизации}
                                        {--all : Создать миграцию, модель, представление, контроллер и файл маршрутизации}
                                        {--migration : Создать только миграцию, контроллер и файл маршрутизации}
                                        {--view :  Создать только представление, контроллер и файл маршрутизации}
                                        {--model :  Создать только модель, контроллер и файл маршрутизации}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Создание модуля';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($this->option('all')) {
            $this->input->setOption('migration', true);
            $this->input->setOption('view', true);
            $this->input->setOption('model', true);
        }

        if ($this->option('migration')) {
            $this->createMigration();
        }

        /** Можно добавить формирование представления */
        if ($this->option('view')) {
            $this->error('Формирование представления в процессе реалиации');
        }

        if ($this->option('model')) {
            $this->createModel();
        }

        $this->createController();
    }


    /**
     * Создание модели
     */
    private function createModel()
    {
        try {
            $model = Str::singular((class_basename($this->argument('name'))));
            $this->call('make:model', [
                'name' => "App\\Modules\\" . trim($this->argument('name')) . "\\Models\\" . $model,
            ]);
        } catch (\Exception $e) {
            $e->getMessage();
        }

    }

    /**
     * Создание миграции
     */
    private function createMigration()
    {
        $table = Str::plural(Str::snake(class_basename($this->argument('name'))));
        try {
            $this->call('make:migration', [
                'name' => 'create_' . $table . '_table',
                '--create' => $table,
            ]);
        } catch (\Exception $e) {
            $e->getMessage();
        }
    }

    /**
     * Создние контроллера
     */
    private function createController()
    {
        $controllerName = Str::studly(class_basename($this->argument('name')));
        $moduleName = Str::singular((Str::studly(class_basename($this->argument('name')))));

        $path = $this->getControllerPath($controllerName);

        $this->makeDirectory($path);

        $stub = $this->files->get(base_path('resources/stubs/controller.model.stub'));

        $stub = str_replace(
            [
                'DummyNamespace',
                'DummyRootNamespace',
                'DummyClass',
                'DummyFullModelClass',
                'DummyModelClass',
                'DummyModelVariable',
            ],
            [
                "App\\Modules\\" . trim($this->argument('name')) . "\\Controllers",
                $this->laravel->getNamespace(),
                $controllerName . 'Controller',
                "App\\Modules\\" . trim($this->argument('name')) . "\\Models\\{$moduleName}",
                $moduleName,
                lcfirst(($moduleName))
            ],
            $stub
        );
        if ($this->alreadyExists($path)) {
            $this->error('Такой контроллер уже существует');
        } else {
            $this->files->put($path, $stub);
            $this->info('Контроллер ' . $controllerName . 'Controller создан');

            $this->createRoutes($controllerName, $moduleName);
        }
    }

    /**
     * Получить имя контроллера
     * @param string $controllerName
     */
    private function getControllerPath(string $controllerName)
    {
        $controller = Str::studly(class_basename($controllerName));
        return $this->laravel['path'] . '/Modules/' . str_replace('\\', '/', $controllerName) . "/Controllers/" . "{$controller}Controller.php";
    }

    /**
     * Создание директории с контроллером
     * @param string $path
     */
    private function makeDirectory(string $path)
    {
        $this->files->makeDirectory(dirname($path), 777, true, true);
    }

    /**
     * Создание файла маршрутизации
     * @param string $controllerName
     * @param string $moduleName
     * @return bool
     */
    private function createRoutes(string $controllerName, string $moduleName)
    {
        $routePath = $this->getRoutesPath($this->argument('name'));

        if ($this->alreadyExists($routePath)) {
            $this->error('Такой файл маршрутизации уже существует');
            return false;
        } else {
            $this->makeDirectory($routePath);

            $stub = $this->files->get(base_path('resources/stubs/routes.admin.stub'));

            $stub = str_replace(
                [
                    'DummyClass',
                    'DummyRoutePrefix',
                    'DummyModelVariable',
                    'DummyModelVariableNameSpace',
                ],
                [
                    $controllerName . 'Controller',
                    Str::plural(Str::snake(lcfirst($moduleName), '-')),
                    lcfirst($moduleName),
                    ucfirst($moduleName),
                ],
                $stub
            );
            $this->files->put($routePath, $stub);
            $this->info('Роут для модуля ' . $moduleName . ' создан');
            return true;
        }
    }

    /**
     * Получить путь до файла маршрутизации
     * @param string $argument
     * @return string
     */
    private function getRoutesPath(string $argument): string
    {
        return $this->laravel['path'] . '/Modules/' . str_replace('\\', '/', $argument) . "/Routes/" . "admin.php";
    }


    /**
     * Проверка на существование файла
     * @param string $routePath
     * @return bool
     */
    private function alreadyExists(string $routePath): bool
    {
        return $this->files->exists($routePath);
    }
}
