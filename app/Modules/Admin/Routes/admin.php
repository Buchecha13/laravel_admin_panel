<?php

use Illuminate\Support\Facades\Route;
use App\Modules\admin\Controllers\AdminController;


Route::get('/', [AdminController::class, 'index'])->name('admins.index');

