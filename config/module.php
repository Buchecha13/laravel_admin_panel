<?php

return [
    /** Путь до модулей */
    'path' => base_path('/app/Modules/'),

    /** Перечень модулей */
    'modules' => [
        'Admin'
    ]
];
