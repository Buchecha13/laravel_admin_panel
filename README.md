<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Формирование модуля

Для формирование модуля используйте консольную команду: php artisan make:module {name}.
Команда содержит следующие опции:

- --all : Создать миграцию, модель, представление, контроллер и файл маршрутизации.
- --migration : Создать только миграцию, контроллер и файл маршрутизации.
- --view :  Создать только представление, контроллер и файл маршрутизации.
- --model :  Создать только модель, контроллер и файл маршрутизации.

<p>Все созданные модули будут по умолчанию находится в групе маршрутов admin.</p>
<p>Настройки маршрутизации можно посмотреть в RouteServiceProvider классе.</p>

<p>Файл конфигурации находится в /config/module.php.</p>
<strong style="color:#cb1010">ВАЖНО!</strong><br>
<strong>После добавления модуля, необходимо записать его наименование в файл конфигурции</strong>
